# Possible

Responsive HTML page with CSS and Js.

## Estimates

- 10 to 12 hours to develop a functional, responsive and a cross-browser html page.

## Features

- Slider built with [Tiny Slider](https://github.com/ganlanyuan/tiny-slider). Vanilla javascript slider for all purposes.
- Responsive design with one breakpoint at 1024 pixels.
- Naming convention based on [BEM CSS methodoly](https://en.bem.info/methodology/css/) for CSS classes.
- Scalable and maintanble CSS architecture based on [ITCSS](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/).
- Inline SVG to reduce http resquests.
- Stylelint and editorconfig files provided to keep consistency in different development platforms/editors.

## Requirements

- NodeJs
- Yarn
- Gulp Js

## Usage

- Clone and cd into the repository folder.
- Open a terminal window and run 'yarn install'.
- Run `gulp` to compile and launch the development local environment.

## TODO

- Create gulp task to build dist files with minification and ready to deploy.
- Update SVG inline code for missing icons.
- Use responsive image tags with different srcset attributes.
- Responsive menu with toggle, CSS animations and missing sliders.
- Missing hero slider styles for each one, alignments, background colors, etc.
- Missing hover interactions for buttons, sliders and images.