(function() {
  var slider = tns({
    container: '.hero__slider',
    items: 1,
    controls: true,
    controlsText: ['Previous', 'Next'],
    controlsContainer: '.hero__slider__controls',
    slideBy: 'page',
    autoplay: false,
    nav: true,
    navContainer: '.hero__slider__nav__inner',
    arrowKeys: true,
    loop: false,
    lazyload: true
  });
})();
